<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
if (!isset($_SESSION["username"])) {
    // in case the user tries to access this page without first signing in with a username
    // they will need redirected to the page that allows them to select a username
    $_SESSION["message"] = "You must be signed in under a username before trying to add locations.";
    $webApp -> RedirectTo("select_username.php");
}

if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $requiredFields = array("city", "country");
    ValidatePresences($requiredFields);

    $fieldsWithMaxLengths = array("city" => 100, "state_province" => 3, "country" => 3);
    ValidateMaxLengths($fieldsWithMaxLengths);

    if (empty($errors)) {

        // form is filled out without errors, but now i need to check and make sure this location is valid
        // the api outputs data in json, and requires the user to enter a city.
        // i noticed that, while playing around with the data, if i enter a fake city name with a legit country code,
        // then the api still returns some weather data. idk what city it is for, but its definitely
        // not what i want the user to see.

        // my idea here is, to check each individual field separately with the api, if they cannot pull data by
        // themselves, then the location will be flagged as invalid.

        // for example the user tries to say their city is "NotARealCity", and country as "US", the api will output
        // weather results for the country "US". But if i did separate checks for "NotARealCity", and "US", the
        // "NotARealCity" will return an error code ins json.

        // the goal here is to prevent users from entering invalid cities. since i cannot get access to
        // open weather map's list of valid cities, this is my next best solution.

        // note that this still doesn't prevent user from entering data such as "us,us,us" for their location.
        // for the most part though, this should stop some invalid data. depending on time i would like to
        // come back and mess around with the open weather api to determine all valid locations, and use that info
        // to provide dynamic auto-complete features to the form as the user types out their location.

        // function to check and make sure if all the fields are valid
        $cityIsValid = ValidateLocationData($_POST["city"]);
        $countryIsValid = ValidateLocationData($_POST["country"]);

        // if all these locations are valid, perform the create
        if ($cityIsValid && $countryIsValid) {
            // city and country are confirmed to be valid, begin setting up query
            $username = $_SESSION["username"];
            $city = strtoupper($webApp -> MySQLPrep($_POST["city"]));
            $country = strtoupper($webApp -> MySQLPrep($_POST["country"]));

            $query  = "INSERT INTO locations (";

            // check to see if user submitted something for the state_province field
            if (!empty($_POST["state_province"])) {
                // if the user did, check to see if the location is valid
                // if the location is valid, proceed with query, else flag the error
                $stateIsValid = ValidateLocationData($_POST["state_province"]);
                if ($stateIsValid) {
                    $aStateOrProvince = strtoupper($webApp -> MySQLPrep($_POST["state_province"]));

                    $locationStringID =
                        strtoupper($webApp -> MySQLPrep($username . "--" . $city . "--" . $aStateOrProvince .
                            "--" . $country));

                    $query .= "location_id, username, city, state_province, country";
                    $query .= ") VALUES (";
                    $query .= "'{$locationStringID}', '{$username}', '{$city}', '{$aStateOrProvince}', '{$country}'";
                    $query .= ")";
                }
                else {
                    // location for state_province was invalid
                    $_SESSION["message"] = "The State or Province was invalid." . PHP_EOL;
                }
            }
            // if the user did not submit something for state_province, proceed with query without state_province
            else {
                $locationStringID =
                    strtoupper($webApp -> MySQLPrep($username . "--" . $city . "--" . $country));

                $query .= "location_id, username, city, country";
                $query .= ") VALUES (";
                $query .= "'{$locationStringID}', '{$username}', '{$city}', '{$country}'";
                $query .= ")";
            }

            // check results of mysql query
            $result = mysqli_query($connection, $query);
            if ($result) {
                // Success
                $_SESSION["message"] = "Successfully added a new location.";
                $webApp -> RedirectTo("index.php");
            }
            else {
                // Failure
                $_SESSION["message"] = "Failed to add a new location." . PHP_EOL;
            }
        }
        else {
            // location for city or state was invalid
            $_SESSION["message"] = "The City or Country was invalid." . PHP_EOL;
        }
    }
}
?>

<?php include("../includes/layouts/header.html"); ?>
<div id="wrapper">

    <?php include("../includes/layouts/sidebar_layout.php"); ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Current Weather App</h2>
                    <?php echo message(); ?>
                    <?php echo $webApp -> GetFormErrors($errors); ?>
                    <h4>Add New Location</h4>
                    <form action="add_location.php" method="post">
                        <p>
                            City Name: <input type="text" name="city" value="" />
                        </p>
                        <p>
                            State/Province Code (Optional): <input type="text" name="state_province" value="" />
                        </p>
                        <p>
                            Country Code: <input type="text" name="country" value="" />
                        </p>
                        <input type="submit" name="submit" value="Add Location" />
                        <a href="index.php">Cancel</a>
                        <br /><br />
                    </form>
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>


<?php include("../includes/layouts/footer.html"); ?>
