<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
if (!isset($_SESSION["username"])) {
    // in case the user tries to access this page without first signing in with a username
    // they will need redirected to the page that allows them to select a username
    $_SESSION["message"] = "To delete a user, sign in under that username and click 'Delete User' in the sidebar.";
    $webApp -> RedirectTo("select_username.php");
}

// query that will find the id and delete the user from the users table
// this will only delete the row for the currently signed in user
// all username are unique, so this will only delete one user from the database

$usernameToBeDeleted = $webApp -> MySQLPrep($_SESSION["username"]);

$deleteUserQuery = "DELETE FROM users WHERE username = '{$usernameToBeDeleted}'";

$deleteUserResult = mysqli_query($connection, $deleteUserQuery);
if ($deleteUserResult && mysqli_affected_rows($connection) == 1) {
    // Success, now set the current username to null
    $_SESSION["username"] = null;

    // now that the username has been delete, i need to clean up the locations table

    $deleteLocationQuery = "DELETE FROM locations WHERE username = '{$usernameToBeDeleted}'";

    $deleteLocationsResult = mysqli_query($connection, $deleteLocationQuery);
    if ($deleteLocationsResult) {
        // Success
        $_SESSION["message"] = "Successfully deleted {$usernameToBeDeleted} and their saved locations.";
    }
}
else {
    // Failure
    $_SESSION["message"] = "Failed to delete {$usernameToBeDeleted} and their saved locations.";
}

// after the query executes, redirect to index php informing the user if the delete was successful or not
$webApp -> RedirectTo("index.php");
