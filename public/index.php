<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/layouts/header.html"); ?>

<div id="wrapper">

    <?php include("../includes/layouts/sidebar_layout.php"); ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Current Weather App</h2>
                    <?php echo message(); // message set in session.php?>
                    <h4>How To:</h4>
                    <ol>
                        <li>
                            Sign in by selecting a username, or create a new username
                        </li>
                        <li>
                            Select the location from the side bar on the left that you would like to view
                            the current weather for, or add a new location to your personalized list.
                        </li>
                        <li>
                            To toggle the sidebar and allow more screen space for weather data, click the
                            "Toggle Menu" button located just under the current weather data for the selected location.
                        </li>
                        <li>
                            To delete a location from your list, click the "Delete Location" button, located under
                            the toggle button.
                        </li>
                        <li>
                            To delete a username, make sure you are signed in as that user and then click the
                            "Delete This User" link, located at the bottom of the sidebar navigation.
                        </li>
                        <li>
                            To switch from one user to another, go to "Select Username" in the sidebar and
                            select another username to sign in as.
                        </li>
                    </ol>
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                    <br/><br/>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>

<?php include("../includes/layouts/footer.html"); ?>