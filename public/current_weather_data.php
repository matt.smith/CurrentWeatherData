<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php include("../includes/layouts/header.html"); ?>

    <div id="wrapper">

        <?php include("../includes/layouts/sidebar_layout.php"); ?>
        <?php
        if (!isset($_GET["location"])) {
            // in case the user tries to access this page without first adding a location,
            // they will need redirected to the page that allows them to add a new location.
            $_SESSION["message"] = "You must be signed in under a username before trying to view current weather.";
            $webApp -> RedirectTo("add_location.php");
        }
        $selectedLocation = $_GET["location"];

        // here i will get the data from the api
        $api_url = "http://api.openweathermap.org/data/2.5/weather?q=" . $selectedLocation . "&appid=44db6a862fba0b067b1930da0d769e98";
        $current_weather_data = file_get_contents($api_url);
        $json = json_decode($current_weather_data, TRUE);

        // here i will try store some relevant data i want from the api
        // if unsuccessful i will simply say the data is unavailable.

        // note that this is getting the current temp in Kelvin, I want Fahrenheit and Celsius
        try {
            $kelvinTemp = $json['main']['temp'];
        } catch (Exception $e) {
            $kelvinTemp = "Unavailable";
        }
        if ($kelvinTemp != "Unavailable") {
            $celsiusTemp = $kelvinTemp-273.15;
            $fahrenheitTemp = $celsiusTemp*(9/5)+32;
        }
        else {
            $celsiusTemp = "Unavailable";
            $fahrenheitTemp = "Unavailable";
        }

        // get data for current humidity
        try {
            $humidity = $json['main']['humidity'];
        } catch (Exception $e) {
            $humidity = "Unavailable";
        }

        // get data for current weather condition
        try {
            $condition = $json['weather'][0]['main'];
        } catch (Exception $e) {
            $condition = "Unavailable";
        }

        // get data for current wind speed
        try {
            $windSpeed = $json['wind']['speed'];
        } catch (Exception $e) {
            $windSpeed = "Unavailable";
        }

        // get data for current wind degree
        try {
            $windDirection = $json['wind']['deg'];
        } catch (Exception $e) {
            $windDirection = "Unavailable";
        }

        // going to figure out if the wind direction for the current location
        // this list shows the cardinal direction equivalent in angles,
        // for this web app I only want to display wind speed using cardinal directions

        /*
         * N        348.75 - 11.25
         * NNE      11.25 - 33.75
         * NE       33.75 - 56.25
         * ENE      56.25 - 78.75
         * E        78.75 - 101.25
         * ESE      101.25 - 123.75
         * SE       123.75 - 146.25
         * SSE      146.25 - 168.75
         * S        168.75 - 191.25
         * SSW      191.25 - 213.75
         * SW       213.75 - 236.25
         * WSW      236.25 - 258.75
         * W        258.75 - 281.25
         * WNW      281.25 - 303.75
         * NW       303.75 - 326.25
         * NNW      326.25 - 348.75
        */

        // I will store the above numbers into an assoc array
        $cardinalDirections = array(
            'N' => array(326.25, 11.25),
            'NE' => array(11.25, 56.25),
            'E' => array(56.25, 101.25),
            'SE' => array(101.25, 146.25),
            'S' => array(146.25, 191.25),
            'SW' => array(191.25, 236.25),
            'W' => array(236.25, 281.25),
            'NW' => array(281.25, 326.25)
        );

        // I will loop through each cardinal direction and check to see which is correct
        if ($windDirection != "Unavailable") {
            foreach ($cardinalDirections as $cd => $angles) {
                // north direction has special logic and needs its own conditionals
                if ($cd == 'N') {
                    try {
                        if ($json['wind']['deg'] >= 0 && $json['wind']['deg'] < $angles[1]) {
                            $windDirection = $cd;
                            break;
                        }
                        else if ($json['wind']['deg'] >= 326.25) {
                            $windDirection = $cd;
                            break;
                        }
                    } catch(Exception $e) {
                        echo "Unavailable" . PHP_EOL;
                    }
                }
                // all other cardinal directions will follow this conditional logic
                else {
                    if ($json['wind']['deg'] >= $angles[0] && $json['wind']['deg'] < $angles[1]) {
                        $windDirection = $cd;
                        break;
                    }
                }
            }
        }
        ?>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Current Weather App</h2>
                        <?php echo message(); ?>
                        <h4>Selected Location: <?php echo $selectedLocation; ?></h4>
                        <ul>
                            <li>
                                Current Temperature: <?php echo $fahrenheitTemp; ?> &#8457 / <?php echo $celsiusTemp; ?>
                                 &#8451
                            </li>
                            <li>
                                Current Humidity: <?php echo $humidity; ?>%
                            </li>
                            <li>
                                Current Condition: <?php echo $condition; ?>
                            </li>
                            <li>
                                Current Wind Speed & Direction: <?php echo $windSpeed; ?> m/s <?php echo $windDirection; ?>
                            </li>
                        </ul>
                        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a><br /><br />
                        <a href="delete_location.php?location=<?php echo urlencode(($selectedLocation)); ?>"
                           class="btn btn-default"
                           onclick="return confirm('Are you sure you want to delete this location?');">Delete Location
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

<?php include("../includes/layouts/footer.html"); ?>