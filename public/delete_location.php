<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
if (!isset($_SESSION["username"])) {
    // in case the user tries to access this page without first signing in with a username
    // they will need redirected to the page that allows them to select a username
    $_SESSION["message"] = "You must be signed in under a username before trying to delete locations.";
    $webApp -> RedirectTo("select_username.php");
}

// need to get the location exploded into an array
$locationArray = explode(',', $_GET["location"]);

// this will be the unique id in the database table
$locationStringID = $_SESSION["username"] . "--";

// if the count of the array is 2, then there is only a city and country saved in the row
if (count($locationArray) == 2) {
    $locationStringID .= $locationArray[0] . "--" . $locationArray[1];
}
// else, there is a city, state_province, and country saved in the row
else {
    $locationStringID .= $locationArray[0] . "--" . $locationArray[1] . "--" . $locationArray[2];
}

// query that will find the id and delete the location
// this will only delete the location for the currently signed in user
$query = "DELETE FROM locations WHERE location_id = '{$locationStringID}'";

$result = mysqli_query($connection, $query);
if ($result && mysqli_affected_rows($connection) == 1) {
    // Success
    $_SESSION["message"] = "Location was deleted";
}
else {
    // Failure
    $_SESSION["message"] = "Location deletion failed.";
}

// after the query executes, redirect to index php informing the user if the delete was successful or not
$webApp -> RedirectTo("index.php");
