<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
if (isset($_POST['submit'])) {
    // Process the form

    // validations
    $requiredFields = array("username");
    ValidatePresences($requiredFields);

    $fieldsWithMaxLengths = array("username" => 15);
    ValidateMaxLengths($fieldsWithMaxLengths);

    if (empty($errors)) {
        // Perform Create
        $username = strtolower($webApp -> MySQLPrep($_POST["username"]));

        $query  = "INSERT INTO users (";
        $query .= "username";
        $query .= ") VALUES (";
        $query .= "'{$username}'";
        $query .= ")";
        $result = mysqli_query($connection, $query);

        if ($result) {
            // Success
            $_SESSION["message"] = "Successfully created a new user.";
        }
        else {
            // Failure
            $_SESSION["message"] = "Failed to create a new user. Make sure the username doesn't already exist.";
        }
        $webApp-> RedirectTo("select_username.php");
    }
}
?>

<?php include("../includes/layouts/header.html"); ?>
<div id="wrapper">

    <?php include("../includes/layouts/sidebar_layout.php"); ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Current Weather App</h2>
                    <?php echo message(); ?>
                    <?php echo $webApp-> GetFormErrors($errors); ?>
                    <h4>Create New User</h4>
                    <form action="new_user.php" method="post">
                        <p>
                            Username: <input type="text" name="username" value="" />
                        </p>
                        <input type="submit" name="submit" value="Create User" />
                        <a href="select_username.php">Cancel</a>
                        <br /><br />
                    </form>

                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>


<?php include("../includes/layouts/footer.html"); ?>
