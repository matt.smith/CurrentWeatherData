<?php require_once("../includes/session.php"); ?>
<?php require_once("../includes/db_connection.php"); ?>
<?php require_once("../includes/functions.php"); ?>
<?php require_once("../includes/validation_functions.php"); ?>

<?php
// default this to null to avoid errors
$username = "";

if (isset($_POST['username'])) {
    // Process the form

    // validations
    $requiredFields = array("username");
    ValidatePresences($requiredFields);   // just a precaution, shouldn't be needed

    if (empty($errors)) {
        // Attempt to use app as the selected username

        $username = $_POST["username"];
        $result = $webApp -> AttemptToLoginAsUsername($username);

        if ($result = $users -> FindUserByUsername($username)) {
            // Success
            // The user has logged in, the session needs to keep this information stored
            // Some parts of the app will require the user to be logged in, so if this isn't saved certain areas are unavailable
            $_SESSION["username"] = $result["username"];
            $webApp-> RedirectTo("index.php");
        }
        else {
            // Failure
            $_SESSION["message"] = "Username not found.";       // shouldn't ever actually happen.
        }
    }
}
?>

<?php include("../includes/layouts/header.html"); ?>

<div id="wrapper">

    <?php include("../includes/layouts/sidebar_layout.php"); ?>

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Current Weather App</h2>
                    <?php echo message(); ?>
                    <?php echo $webApp -> GetFormErrors($errors); // just a precaution, shouldn't happen in practice?>
                    <h4>Select Username Below</h4>
<!--                    here i need to add the drop down menu for the list of users-->
                    <form action="select_username.php" method="post">
                        Username:
                        <select name="username">
                            <?php $users -> ProvideDropDownBoxForUsernames(); ?>
                        </select>
                        <br /><br />
                        <input type="submit" name="submit" value="Select Username" />
                    </form>
                    <br />
                    <a href="new_user.php">Click here to create a new username</a>
                    <br /><br />
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
                    <br/><br/>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>

<?php include("../includes/layouts/footer.html"); ?>
