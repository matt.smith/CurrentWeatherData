-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2016 at 04:30 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `weather_data`
--

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` varchar(129) NOT NULL DEFAULT '',
  `username` varchar(15) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `state_province` varchar(3) DEFAULT NULL,
  `country` varchar(3) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_id`, `username`, `city`, `state_province`, `country`) VALUES
('FELICIA-MORROW7--CHICAGO--IL--USA', 'felicia-morrow7', 'CHICAGO', 'IL', 'USA'),
('FELICIA-MORROW7--LAS VEGAS--NV--USA', 'felicia-morrow7', 'LAS VEGAS', 'NV', 'USA'),
('FELICIA-MORROW7--LONDON----UK', 'felicia-morrow7', 'LONDON', NULL, 'UK'),
('FELICIA-MORROW7--PARIS----FR', 'felicia-morrow7', 'PARIS', NULL, 'FR'),
('FELICIA-MORROW7--SOMERSET--KY--USA', 'felicia-morrow7', 'SOMERSET', 'KY', 'USA'),
('MATTHEW_SMITH--LEXINGTON--KY--USA', 'matthew_smith', 'LEXINGTON', 'KY', 'USA'),
('MATTHEW_SMITH--SOMERSET--KY--USA', 'matthew_smith', 'SOMERSET', 'KY', 'USA'),
('NICK123--LEXINGTON--KY--USA', 'nick123', 'LEXINGTON', 'KY', 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`) VALUES
(3, 'felicia-morrow7'),
(1, 'matthew_smith'),
(2, 'nick123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
