This directory contains two .sql files.

Both of these files contain the database needed for the web application.

If you are wanting to clone this repository and take a look at the code
and mess around with the web application, you will need to setup a MySQL
database. You will also need to create a database user who has permissions
to interact with the database.

Refer to includes/db_connection.php to see the credentials I used for
the server, user, password, and database name.

Once you have setup a MySQL database for this web application, you can use
either one of these files to populate the database. I provided one that
contains data I used when developing this web application. The other file
will contain the same tables, but with no data within them. This may be of
preference to you. Either is fine. Both should work.

If for whatever reason you cannot import these database tables, here are
some SQL statements to get you up and running:


// for the users table
CREATE TABLE users (
	id int(11) AUTO_INCREMENT,
	username varchar(15) UNIQUE,
	PRIMARY KEY (id)
);



// for the locations table
CREATE TABLE locations (
	location_id varchar(129),
	username varchar(15),
	city varchar(100) NOT NULL,
	state_province varchar(3),
	country varchar(3) NOT NULL,
	PRIMARY KEY (location_id)
);
