<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="index.php?About">About This App</a>
        </li>
        <li>
            <a href="select_username.php">Select Username</a>
        </li>
        <li>
            <?php
            if (!isset($_SESSION["username"])) {
                $username = "None Selected";
            }
            else {
                $username = $_SESSION["username"];
            }
            ?>
            Current User: <?php echo $username; ?>
            <br /><br />
        </li>

        <!-- Below is a list of all locations that are stored in the database for the current user,
        if a username is selected.-->
        <?php $users -> DisplayLocationsInSidebarForCurrentUsername($username); ?>
<!--        if the user is currently signed in under a username, then provide the add location-->
<!--         and delete option here. otherwise, this isn't necessary since the user is not signed in-->
        <?php if (isset($_SESSION["username"])): ?>
            <li>
                <a href="add_location.php">Add a location +</a>
            </li>
            <br /><br /><br /><br />
            <li>
                <a href="delete_user.php"onclick="return confirm('Deleting this user will delete all saved locations ' +
                 'for this user.');">
                    Delete This User
                </a>
            </li>
        <?php endif; ?>
    </ul>
</div>
<!-- /#sidebar-wrapper -->