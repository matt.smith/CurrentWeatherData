<?php
$errors = array();      // array containing errors

/**
 * @param $fieldname
 * @return mixed|string
 *
 * this function will determine if a field has text in it or not
 */
function FieldAsText($fieldname) {
    $fieldname = str_replace("_", " ", $fieldname);
    $fieldname = ucfirst($fieldname);
    return $fieldname;
}

/**
 * @param $value
 * @return bool
 *
 * this function will determine if the parameter is set or not
 */
function HasPresence($value) {
    return isset($value) && $value !== "";
}

/**
 * @param $required_fields
 *
 * this function will validate forms that are submitted by users. if the forms are not valid, then messages are passed
 * into the errors array and will be echoed out to the page for the user to see.
 */
function ValidatePresences($required_fields) {
    global $errors;
    foreach($required_fields as $field) {
        $value = trim($_POST[$field]);
        if (!HasPresence($value)) {
            $errors[$field] = FieldAsText($field) . " can't be blank.";
        }
    }
}

/**
 * @param $value
 * @param $max
 * @return bool
 *
 * this function will compare if a given value is less than or equal to the specified value for 'max'
 * if the provided value is less than the max length, function will return true, otherwise it'll return false
 *
 * this function will be used along with another function to validate particular value's max length
 */
function IsLessThanMaxLength($value, $max) {
    return strlen($value) <= $max;      // will return true or false depending on the outcome of this comparison
}

/**
 * @param $fieldsWithMaxLengths
 *
 * this function will validate whether or not the fields in the array are less than the specified max length
 *
 * similar logic to the ValidatePresences function
 */
function ValidateMaxLengths($fieldsWithMaxLengths) {
    global $errors;
    foreach($fieldsWithMaxLengths as $field => $max) {
        $value = trim($_POST[$field]);
        if (!IsLessThanMaxLength($value, $max)) {
            $errors[$field] = FieldAsText($field) . " is too long";
        }
    }
}

/**
 * @param $location
 * @return bool
 *
 * this will simply validate that the location the user is trying to add actually exists within the
 * openweathermap api. if the location does not exist, inform the user that the location is invalid.
 */
function ValidateLocationData($location) {

    // checks to see if this data can be pulled from the open weather api, or if the error code 400 is returned.
    // if we can get data, this function returns True, since the location must be valid.
    // if we cannot get data, this function returns False, since the location must not be valid

    // here i will get the data from the api using the the format specified by open weather map's documentation
    $apiURL = "http://api.openweathermap.org/data/2.5/weather?q=" . $location . "&appid=44db6a862fba0b067b1930da0d769e98";
    $currentWeatherData = file_get_contents($apiURL);
    json_decode($currentWeatherData, TRUE);

    // after playing around with this api I learned that if a location cannot be found, this error code will be
    // returned in json. after using json_decode, if this error code is in the data then that means that the
    // location parameter most likely does not exist, or that open weather simply doesn't provide data for this
    // location. Either way, if this code is found then return false, otherwise return true.

    // this will help with validating location data whenever a user fills out a form for adding a new location to
    // be tracked for current weather.
    if (strpos($currentWeatherData, "\"cod\":\"404\"")) {
        return False;
    }
    else {
        return True;
    }
}
