<?php

class Usernames {

    /**
     * @param $username
     *
     * this function will display locations in the sidebar div for whatever user is currently signed in
     *
     */
    function DisplayLocationsInSidebarForCurrentUsername($username) {
        global $connection;

        // need to get every username from the users table
        $query = "SELECT * FROM locations WHERE username='$username' ORDER BY city ASC, country ASC, state_province ASC";

        $result = mysqli_query($connection, $query); // This line executes the query

        $locationArray = []; // make a new array to hold every location

        $index = 0;
        while($row = mysqli_fetch_assoc($result)){ // loop to store the data in an associative array.
            $locationArray[$index] = $row;
            $index++;
        }

        // now loop through each location and output in the sidebar
        for ($i = 0; $i < count($locationArray); $i++) {
            echo "<li>";

            // here i am echoing the city,state-province(if applicable), and country into the URL and storing it into
            // "location", which will be accessed through the $_GET method on current_weather_data.php
            echo '<a href="current_weather_data.php?';
            echo 'location=' . urlencode(str_replace(' ', '', $locationArray[$i]["city"])) . ',';
            // check to see is state/province is specified
            if (!is_null($locationArray[$i]["state_province"])) {
                echo urlencode(str_replace(' ', '', $locationArray[$i]["state_province"])) . ',';
            }
            echo urlencode(str_replace(' ', '', $locationArray[$i]["country"])) . '">';

            // here i am echoing the city, state-province(if applicable), and country into the sidebar, allowing the
            // user to click on the location and see the current weather for that location.
            echo $locationArray[$i]["city"] . ', ';
            // check to see is state/province is specified
            if (!is_null($locationArray[$i]["state_province"])) {
                echo $locationArray[$i]["state_province"] . ', ';
            }
            echo $locationArray[$i]["country"];
            echo '</a>';

            echo "</li>";
        }
    }

    /**
     * @param $username
     * @return array|null
     *
     * this function will find a user in the database based on their chosen username.
     * this function will call the ConfirmQuery function from the WebApplication Class.
     */
    function FindUserByUsername($username) {
        global $webApp;
        global $connection;
        $safeUsername = mysqli_real_escape_string($connection, $username);

        $query  = "SELECT * ";
        $query .= "FROM users ";
        $query .= "WHERE username = '{$safeUsername}' ";
        $query .= "LIMIT 1";
        $usernameSet = mysqli_query($connection, $query);
        $webApp -> ConfirmQuery($usernameSet);
        if ($username = mysqli_fetch_assoc($usernameSet)) {
            return $username;
        }
        else {
            return null;
        }
    }

    /**
     * this function will provide a drop down box for the usernames that are stored in the database
     * the user will be able to select the username they wish to sign in as.
     *
     * this function is very similar to the DisplayLocationsInSidebarForCurrentUsername function.
     */
    function ProvideDropDownBoxForUsernames() {
        global $connection;

        // need to get every username from the users table
        $query = "SELECT * FROM users ORDER BY id ASC";

        $result = mysqli_query($connection, $query); // This line executes the query

        $usernameArray = []; // make a new array to hold every username

        $index = 0;
        while($row = mysqli_fetch_assoc($result)){ // loop to store the data in an associative array.
            $usernameArray[$index] = $row;
            $index++;
        }

        // now loop through each username and output in a drop box
        for ($i = 0; $i < count($usernameArray); $i++) {
            echo "<option>";
            echo $usernameArray[$i]["username"];
            echo "</option>";
        }
    }
}

class WebApplication {

    /**
     * @param $username
     * @return array|bool|null
     *
     * this function will attempt to log the user in as the selected username they chose
     * on the select_username.php page.
     */
    function AttemptToLoginAsUsername($username) {
        // check to see if database contains the username

        global $users;

        $user = $users -> FindUserByUsername($username);

        if ($user) {
            // the user was found
            return $user;
        }
        else {
            // the user was not found
            return False;
        }
    }

    /**
     * @param $newLocation
     *
     * this function will simply redirect the user to a different page, which will be useful
     * for me to keep users off of pages they do not belong at without certain criteria
     *
     * for example, if the user isn't signed in with a username and they try to "add a location",
     * they will be redirected to the select_username.php page.
     *
     * another example, would be if i wanted to redirect the user to the index.php page
     */
    function RedirectTo($newLocation) {
        header("Location: " . $newLocation);
        exit;
    }

    /**
     * @param array $errors
     * @return string
     *
     * this function will get the form errors that were submitted by the user, and output them onto the page.
     */
    function GetFormErrors($errors = array()) {
        $output = "";
        if (!empty($errors)) {
            $output .= "<div class=\"error\">";
            $output .= "Please fix the following errors:";
            $output .= "<ul>";
            foreach ($errors as $key => $error) {
                $output .= "<li>";
                $output .= htmlentities($error);
                $output .= "</li>";
            }
            $output .= "</ul>";
            $output .= "</div>";
        }
        return $output;
    }

    /**
     * @param $resultSet
     *
     * this function will confirm the query used to interact with the database
     */
    function ConfirmQuery($resultSet) {
        if (!$resultSet) {
            die("Database query failed.");
        }
    }

    /**
     * @param $string
     * @return string
     *
     * this function will prepare the string for use in an SQL statement,
     * taking into account the current charset of the connection
     *
     * saves myself from typing this stuff repeatedly on multiple pages.
     */
    function MySQLPrep($stringValue) {
        global $connection;

        $escapedString = mysqli_real_escape_string($connection, $stringValue);
        return $escapedString;
    }
}

$users = new Usernames();
$webApp = new WebApplication();
