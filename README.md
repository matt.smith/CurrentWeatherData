This web application allows you to add cities that you would like to view the
current weather for. You will first need to sign in by selecting a username, or
create one if you have not already done so. No authentication is necessary,
the username is used to simply keep track of locations that you would like to
know the current weather conditions for. Clicking on "Select Username" will help
get you started with using the web application.

Once you have specified who you are, the sidebar on the left hand side of the
screen will contain a list of locations that you have previously looked up in
the past. If you have not previously looked up a location's current weather, you
will not see a list until after you add a location.

You can add as many locations as you like, and they will appear in ascending
order, sorted by city name. You can delete these locations from your list by
clicking the "Delete Location" button under the "Toggle Menu" button, should you
decide that you no longer need to view this location's current weather data.
You can use the toggle button to toggle the left hand sidebar, in case you
prefer viewing the current weather data for your selected location on the entire
screen of your device.

If you are currently using the application under one username, and would like to
switch to another, you can switch to another username at any time by simply
clicking on "Select Username".

If you would like to delete a username that you never use anymore, you must
first sign in as that username and then you will see a link at the bottom of the
sidebar that says "Delete This User". Click that link and you'll be prompted
with a confirmation window. Click the OK button and the username will be deleted
along with any saved locations associated with that username.


This web application retrieves current weather data from the openweathermap api.
http://openweathermap.org/api

The sidebar layout was forked from:
https://github.com/BlackrockDigital/startbootstrap-simple-sidebar